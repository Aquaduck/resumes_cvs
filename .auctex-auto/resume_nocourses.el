(TeX-add-style-hook
 "resume_nocourses"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-package-options
                     '(("geometry" "left=0.75in" "top=0.6in" "right=0.75in" "bottom=0.6in")))
   (TeX-run-style-hooks
    "latex2e"
    "resume_edit"
    "resume_edit10"
    "geometry"))
 :latex)

